# UserChrome Breeze 
A simple Breeze theme for Mozilla Firefox.

## Installation
Just copy the `icons` forlder and `userChrome.css` file to your Firefox chrome directory.

## Screenshot 
![screenshot](screenshots/screen.png)